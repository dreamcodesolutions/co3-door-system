// Require Photon
const WebSocket = require('ws');
const Photon = require('./photon.js');
const sql = require('mssql')
var App = {};

window.addEventListener("activate", function(event) {
  console.log(event);
});

window.addEventListener("load", function() {
  var componentGroup = document.getElementsByClassName("component-groups")[0];
  componentGroup.addEventListener("activate", function(event) {
    var viewSelector = event.detail.button.getAttribute("data-view");
    var btns = this.getElementsByTagName("button");
    for (var i = 0; i < btns.length; i++) {
      document.querySelector(btns[i].getAttribute("data-view")).style.display = "none";
    }
    document.querySelector(viewSelector).style.removeProperty("display");
  });

  document.getElementById("status-string").addEventListener("click", function() {
    var value = this.innerHTML;

    if (value == 'Connected'){
      App.ws.close();
    }else if (value == 'Disconnected'){
      // if (App !=null && App.ws !=null){
      //   App.ws.close()
      // }
      App = startConnection();
    }else if (value == 'Connecting'){
      // if (App !=null && App.ws !=null){
      //   App.ws.close()
      // }
      App = startConnection();
    }
    console.log("button Pressed "+value);
  });

  window.addEventListener("resize", function(event) {
    //console.log(event);
  });

  window.addEventListener("input", function(event) {
    console.log(event);
  });
});


var sqlConfig = {
  user: 'seserver',
  password: '11201SEacs',
  server: 'CO3RECEPTIONDES\\SOYALSQL',
  database: 'soyaletegra',
  options: {
    encrypt: false // Use this if you're on Windows Azure
  }
};


var pool;
startSqlPool();
async function startSqlPool(){
   pool=  await sql.connect(sqlConfig);

}
setTimeout(
  function(){ App = startConnection();
 },
  1000
);

setInterval(
  function(){
    if (document.getElementById("status-string").innerHTML != 'Connected'){
       if (App !=null && App.ws !=null){
        App.ws.close()
      }
      App = startConnection();
    }
  },
  5000
);

sql.on('error', err => {
    // ... error handler
    appendToLog(err,'error')
})

function startConnection(){

  const ws =  new WebSocket('ws://app.co3.co:28080/cable?token=a4f6c1d3-2ec2-4072-9c29-75dd1359727a&version=application/co3.v5');
  const App = {};

  App.ws = ws
  updateConnectionStatus('Connecting');

  App.ws.on('open', function open() {
    console.log("open");
    const msg = getSubscribeMessage();

    App.ws.send(msg);
  });

  App.ws.on('message', function incoming(data) {
    console.log(data);
    appendToLog(data,'received')
    updateConnectionStatus('Connected');


    var json = JSON.parse(data)
    var identifier = json.identifier;

    if (identifier != null){

      var message = json.message
      if (message != null ){
        var type = message.type
        if (type == 'command'){
          var reply = message
          reply['type'] = 'acknowledgement'
          reply['to'] = 'doors'
          reply['action'] = 'send_message'
          reply['status'] = 'received'

          const msg = JSON.stringify({
            command: "message",
            data: JSON.stringify(reply),
            identifier: JSON.stringify({
              channel: 'DoorChannel',
            }),
          });

          var id = message._id
          var door = message.door

          App.ws.send(msg);

          appendToLog("hallo",'command')
          // console.log("command detected");

          if (door != null){
            const site_id = door.site_id
            const device_id = door.device_id
            const type = door.type
            const command = message.command
            const parseData = (`command ${command} vs site id ${site_id} vs ${device_id} vs ${type}`);
            // appendToLog(data,'command')
            appendToLog(parseData,'parse')


            update_database(site_id,device_id,command,type,message,App.ws)
          }
        }
      }
    }
  });

  App.ws.on('close', function close() {
    // pool.close()
    appendToLog('disconnected','disconnected')
    console.log('disconnected');
    updateConnectionStatus('Disconnected');
  });

  return App;
}

async function update_database(site_id,device_id,command,type,message,ws) {
    await pool; // ensures that the pool has been created
    try {
      const request = new sql.Request(pool)
      const queryString = `INSERT INTO device_requests(site_id,device_id,details,type,status) Values(${site_id},\'${device_id}\',\'${command}\',${type},0)`;
      const result = request.query(queryString,function(err,result){
         setTimeout(
                function(){ 
                    const request2 =  new sql.Request(pool)
                    const queryString2 = `SELECT TOP 1 * FROM device_requests WHERE site_id = ${site_id} ORDER BY id DESC`;
                    
                       appendToLog({"testing":"2"},'reply socket')
                    const result2 =  request2.query(queryString2,function(err,result){
                           const obj = result.recordsets[0][0];

                            var reply =  JSON.parse(JSON.stringify(message));
                            reply['type'] = 'acknowledgement'
                            reply['to'] = 'doors'
                            reply['action'] = 'send_message'

                            console.log("obj status ",obj.status);
                            if (obj.status == 1){
                              reply['status'] = 'running'
                              console.log("still running")
                            }else if (obj.status == 2){
                              reply['status'] = 'success'
                              console.log("executed successfully")
                            }else  if (obj.status == 3){
                              reply['status'] = 'failed'
                              console.log("executed failed")
                            }

                            const msg = JSON.stringify({
                              command: "message",
                              data: JSON.stringify(reply),
                              identifier: JSON.stringify({
                                channel: 'DoorChannel',
                              }),
                            });
                            appendToLog(msg,'reply socket')
                            App.ws.send(msg);
                    });
               },
                1000
              );

      });


     

    
      
    } catch (err) {
        console.error('SQL error', err);
    }
}


function updateConnectionStatus(status){
  var tableRef = document.getElementById('status-string')
  tableRef.innerHTML = status;
}

function appendToLog(log,type){
  var time = new Date();
  var timeString =  ("0" + time.getHours()).slice(-2)   + ":" + ("0" + time.getMinutes()).slice(-2) + ":" + ("0" + time.getSeconds()).slice(-2)
  var myHtmlContent = `<tr><td>${timeString}</td><td>${type}</td><td>${log}</td></tr> `
  var tableRef = document.getElementById('event-log').getElementsByTagName('tbody')[0];
  var newRow   = tableRef.insertRow(-1);
  newRow.innerHTML = myHtmlContent;
  newRow.scrollIntoView(true);
}

function getSubscribeMessage(){
  const msg = {
    command: 'subscribe',
    identifier: JSON.stringify({
      channel: 'DoorChannel',
    }),
  };

  return JSON.stringify(msg)
}
